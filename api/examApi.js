// ~/api/examApi.js
import axios from 'axios'

export default {
  getExamData () {
    return axios.get('https://demotrade.efintradeplus.com/ExamAPI/examdata')
  }
}
